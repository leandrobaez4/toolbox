var express = require("express");
var app = express();
const cors = require('cors');
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.json());
app.use(express.json());
app.use(cors({ origin: '*'}));

var controllerProcessText = require('./controllers/processText');
var controllers = express.Router();

controllers.route('/processText').post(controllerProcessText.process);

app.use('/api',controllers);

app.listen(5000, function () {
    
});
module.exports = app