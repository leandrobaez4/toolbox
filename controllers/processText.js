exports.process = (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.json({ title: req.body.title, id: req.body.id, message: req.body.message});
}