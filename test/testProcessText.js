//apiTest.js
const request = require('supertest');
const app = require('../app');
const chai = require('chai')
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe("POST /api/processText", () => {

    let data = {
        id: new Date(),
        title: 'test',
        message: 'test'
    }

    it("respond with json containing a process test", done => {
        chai
            .request(app)
            .post("/api/processText")
            .send(data)
            .set('Content-Type', 'application/json')
            .set('Accept', /json/)
            .end((err, res) => {
                chai.expect(res).to.have.status(200);
                chai.expect(data.title).to.equals(res.body.title);
                chai.expect(data.message).to.equals(res.body.message);
                done();
            });
    });    
});

module.exports = app