const postReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_POST':
            return state.concat([action.payload.data])        
        default:
            return state;
    }
}
export default postReducer;