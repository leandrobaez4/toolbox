import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addPost } from "./postActions";
import { Form, Button } from 'react-bootstrap';

class PostForm extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        const title = this.getTitle.value;
        const message = this.getMessage.value;
        const data = {
            id: new Date(),
            title,
            message
        }
        this.props.dispatch(addPost(data));
        this.getTitle.value = '';
        this.getMessage.value = '';
    }

    render() {
        return (
            <div className="post-container">
                <h1 className="post_heading">Crear Post</h1>
                <Form className="form" onSubmit={this.handleSubmit}>
                    <Form.Control
                        required
                        type="text"
                        ref={(input) => this.getTitle = input}
                        placeholder="Titulo"
                    />
                    <br /><br />
                    <Form.Control 
                    required 
                    as="textarea" 
                    rows="5" 
                    ref={(input) => this.getMessage = input}
                    cols="28" 
                    placeholder="Mensaje"
                    />
                    <br /><br />
                    <Button variant="success" type="submit">Crear</Button>
                </Form>
            </div>
        );
    }
}

export default connect()(PostForm);