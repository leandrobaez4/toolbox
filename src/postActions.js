import axios from "axios";

export function addPost(data) {
    return dispatch => {        
        return axios.post("http://localhost:5000/api/processText",data)
            .then(json => {                
                dispatch(fetchProductsSuccess(json.data));                
            })
            .catch(function(error) {
                console.log(error);
            });
    };
}

export const fetchProductsSuccess = data => ({
    type: 'ADD_POST',
    payload: { data }
});