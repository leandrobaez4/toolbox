# Challenger

## Instalación de los paquetes de Node

```bash
npm install
```
## Correr en 2 terminales diferentes los siguientes comandos:

```bash
npm start
```

```bash
node app.js
```    
   
## Correr test de proccesText(supertest):

```bash
npm test
``` 

## Importante

El front de react corre en http://localhost:3000 y node http://localhost:5000

## GIT

ssh
```bash
 git clone git@bitbucket.org:leandrobaez4/toolbox.git
``` 
https
```bash
 git clone https://leandrobaez4@bitbucket.org/leandrobaez4/toolbox.git
```   